from bs4 import BeautifulSoup
import urllib.request

resp = urllib.request.urlopen("https://en.wikipedia.org/wiki/Rome")
soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'), features="lxml")
f = open('links.txt','w+')

for link in soup.find_all('a', href=True):
    f.write(link['href'])