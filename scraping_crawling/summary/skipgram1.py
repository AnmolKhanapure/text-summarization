# import numpy as np 
# import pandas as pd 
# import os
# import sys
# import re
# from numpy.random import seed 
# seed(1)

# with open('file.txt','r') as file:
#     text = file.read()

# import gensim as gs
# import scipy as sc
# import nltk
# from nltk.tokenize import word_tokenize as wt
# from nltk.tokenize import sent_tokenize as st
# from numpy import argmax
# from sklearn.preprocessing import LabelEncoder
# from sklearn.preprocessing import OneHotEncoder
# from keras.preprocessing.sequence import pad_sequences
# import logging
# from collections import Counter

# logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',\
#     level=logging.INFO)

# emb_size_all = 300
# maxcorp=5000

# def cleantext(text):
#     text=re.sub(r"what's","what is ",text)
#     text=re.sub(r"it's","it is ",text)
#     text=re.sub(r"\'ve"," have ",text)
#     text=re.sub(r"i'm","i am ",text)
#     text=re.sub(r"\'re"," are ",text)
#     text=re.sub(r"n't"," not ",text)
#     text=re.sub(r"\'d"," would ",text)
#     text=re.sub(r"\'s","s",text)
#     text=re.sub(r"\'ll"," will ",text)
#     text=re.sub(r"can't"," cannot ",text)
#     text=re.sub(r" e g "," eg ",text)
#     text=re.sub(r"e-mail","email",text)
#     text=re.sub(r"9\\/11"," 911 ",text)
#     text=re.sub(r" u.s"," american ",text)
#     text=re.sub(r" u.n"," united nations ",text)
#     text=re.sub(r"\n"," ",text)
#     text=re.sub(r":"," ",text)
#     text=re.sub(r"-"," ",text)
#     text=re.sub(r"\_"," ",text)
#     text=re.sub(r"\d+"," ",text)
#     text=re.sub(r"[$#@%&*!~?%{}()]"," ",text)
    
#     return text

# def createCorpus(t):
#     corpus = []
#     all_sent = []
#     for k in t:
#         for p in t[k]:
#             corpus.append(st(p))
#     for sent in range(len(corpus)):
#         for k in corpus[sent]:
#             all_sent.append(k)
#     for m in range(len(all_sent)):
#         all_sent[m] = wt(all_sent[m])
    
#     all_words=[]
#     for sent in all_sent:
#         hold=[]
#         for word in sent:
#             hold.append(word.lower())
#         all_words.append(hold)
#     return all_words

# #training the model
# def word2vecmodel(corpus):
#     emb_size=emb_size_all
#     model_type={"skipgram":1,"CBOW":0}
#     window = 10
#     workers = 4
#     min_count = 4
#     batch_words = 20
#     epochs=25

#     model=gs.models.Word2Vec(corpus,size=emb_size,sg=model_type["skip_gram"],
#                              compute_loss=True,window=window,min_count=min_count,workers=workers,
#                              batch_words=batch_words)
    
#     model.train(corpus,total_examples=len(corpus),epochs=epochs)
#     model.save("%sWord2vec")
#     print('\007')
#     return model

# #one hot encoder part
# def summonehot(corpus):
#     allwords = []
#     annotated = {}
#     for sent in corpus:
#         for word in wt(sent):
#             allwords.append(word.lower())
#     print(len(set(allwords)), "unique characters in corpus")
#     maxcorp=int(len(set(allwords))/1.1)
#     wordcount = Counter(allwords).most_common(maxcorp)
#     allwords=[]
    
#     for p in wordcount:
#         allwords.append(p[0])  
        
#     allwords=list(set(allwords))
#     print(len(allwords), "unique characters in corpus after max corpus cut")
#     #integer encode
#     label_encoder = LabelEncoder()
#     integer_encoded = label_encoder.fit_transform(allwords)
#     #one hot
#     onehot_encoder = OneHotEncoder(sparse=False)
#     integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
#     onehot_encoded = onehot_encoder.fit_transform(integer_encoded)
#     #make look up dict
#     for k in range(len(onehot_encoded)): 
#         inverted = cleantext(label_encoder.inverse_transform([argmax(onehot_encoded[k, :])])[0]).strip()
#         annotated[inverted]=onehot_encoded[k]
#     return label_encoder,onehot_encoded,annotated

# def wordvecmatrix(model,data):
#     IO_data={"article":[],"summaries":[]}
#     i=1
#     for k in range(len(data["articles"])):
#         art=[]
#         summ=[]
#         for word in wt(data["articles"][k].lower()):
#             try:
#                 art.append(model.wv.word_vec(word))
#             except Exception as e:
#                 print(e)

#         for word in wt(data["summaries"][k].lower()):
#             try:
#                 summ.append(onehot[word])
#                 #summ.append(model.wv.word_vec(word))
#             except Exception as e:
#                 print(e)
        
#         IO_data["article"].append(art) 
#         IO_data["summaries"].append(summ)
#         if i%100==0:
#             print("progress: " + str(((i*100)/len(data["articles"]))))
#         i+=1
#     #announcedone()
#     print('\007')
#     return IO_data

# def cutoffSequences(data,artLen,sumlen):
#     data2={"article":[],"summaries":[]}
#     for k in range(len(data["article"])):
#         if len(data["article"][k])<artLen or len(data["summaries"][k])<sumlen:
#              #data["article"]=np.delete(data["article"],k,0)
#              #data["article"]=np.delete(data["summaries"],k,0)
#              pass
#         else:
#             data2["article"].append(data["article"][k][:artLen])
#             data2["summaries"].append(data["summaries"][k][:sumlen])
#     return data2


# def max_len(data):
#     lenk=[]
#     for k in data:
#         lenk.append(len(k))
#     print("The minimum length is: ",min(lenk))
#     print("The average length is: ",np.average(lenk))
#     print("The max length is: ",max(lenk))
#     return min(lenk),max(lenk)

# """reshape vectres for Gensim"""
# def reshape(vec):
#     return np.reshape(vec,(1,emb_size_all))

# def addones(seq):
#     return np.insert(seq, [0], [[0],], axis = 0)

# def endseq(seq):
#     pp=len(seq)
#     return np.insert(seq, [pp], [[1],], axis = 0)
# #######################################################################
# #######################################################################

# corpus = createCorpus(text)



# # text=re.sub(r"what's","what is ",text)
# # text=re.sub(r"it's","it is ",text)
# # text=re.sub(r"\'ve"," have ",text)
# # text=re.sub(r"i'm","i am ",text)
# # text=re.sub(r"\'re"," are ",text)
# # text=re.sub(r"n't"," not ",text)
# # text=re.sub(r"\'d"," would ",text)
# # text=re.sub(r"\'s","s",text)
# # text=re.sub(r"\'ll"," will ",text)
# # text=re.sub(r"can't"," cannot ",text)
# # text=re.sub(r" e g "," eg ",text)
# # text=re.sub(r"e-mail","email",text)
# # text=re.sub(r"9\\/11"," 911 ",text)
# # text=re.sub(r" u.s"," american ",text)
# # text=re.sub(r" u.n"," united nations ",text)
# # text=re.sub(r"\n"," ",text)
# # text=re.sub(r":"," ",text)
# # text=re.sub(r"-"," ",text)
# # text=re.sub(r"\_"," ",text)
# # text=re.sub(r"\d+"," ",text)
# # text=re.sub(r"[$#@%&*!~?%{}()]"," ",text)

# print(text)

