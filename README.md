1. For document 2 vector - refer to file word2vec.py at location scraping_crawling/summary
2. For keras lstm - refer to file keras_lstm.py at location /scraping_crawling/ptb



FOR WORD EMBEDDING:
    
    FIRST RUN scraping.py
    THEN word2vec.py
    
    Refer to link - https://medium.com/jatana/unsupervised-text-summarization-using-sentence-embeddings-adb15ce83db1
    
FOR LSTM USING KERAS:
    
    FOR TRAINING: python keras_lstm.py --data_path {path_to_data} 1
    FOR TRAINIG: python keras_lstm.py --data_path {path_to_data} 2
    
    Refer to link: https://adventuresinmachinelearning.com/keras-lstm-tutorial/

